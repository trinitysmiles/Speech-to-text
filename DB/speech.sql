-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июл 30 2018 г., 17:43
-- Версия сервера: 5.7.22-0ubuntu18.04.1
-- Версия PHP: 7.2.8-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `edu_speech`
--

-- --------------------------------------------------------

--
-- Структура таблицы `speech`
--

CREATE TABLE `speech` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `speech`
--

INSERT INTO `speech` (`id`, `filename`, `text`, `created_at`, `updated_at`) VALUES
(7, 'https://storage.googleapis.com/cloud-samples-tests/speech/brooklyn.flac', '', '2018-07-30 12:42:23', '2018-07-30 14:42:46'),
(8, 'https://storage.googleapis.com/speech-language-samples/fr-sample.flac', '', '2018-07-30 12:53:10', '2018-07-30 14:42:48'),
(9, '/data/projects/speech/resources/center1.flac', '', '2018-07-30 12:57:53', '2018-07-30 14:42:50'),
(10, '/data/projects/speech/resources/dv.flac', '', '2018-07-30 13:02:02', '2018-07-30 14:42:58');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `speech`
--
ALTER TABLE `speech`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `speech`
--
ALTER TABLE `speech`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
