<?php
require __DIR__ . '/src/Database.php';
require __DIR__ . '/config.php';


if (isset($_POST['link'])) {
    $db->insert('speech', ['filename' => $_POST['link']]);
    $result = [
        'status' => 1,
        'text' => ''
    ];

} else {
    $result = [
        'status' => 0,
        'text' => ''
    ];
}

header('Content-type: application/json');
echo json_encode($result);