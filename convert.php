<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/src/Database.php';
require __DIR__ . '/config.php';

use Google\Cloud\Speech\SpeechClient;

header('Content-type: application/json');

$result = [
    'status' => 0,
    'text' => 'Invalid query'
];

if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $speechData = $db->select('speech', ['id' => $id])->result();


    if (!empty($speechData)) {
        $projectId = 'speech-1532943030336';
        $speech = new SpeechClient([
            'projectId' => $projectId,
            'languageCode' => 'en-US',
            'keyFilePath' => 'Speech-9a46d98ae4d8.json'
        ]);

        try {
            $results = $speech->recognize(fopen($speechData[0]->filename, 'r'));
        } catch (Exception $e) {
            $result = [
                'status' => 0,
                'text' => 'Incorrect source file'
            ];
        }

        if ($results) {
            $text = '';
            foreach ($results as $result) {
                $text .= $result->alternatives()[0]['transcript'] . PHP_EOL;
            }
            $db->update('speech', ['text' => $text], ['id' => $id]);
            $result = [
                'status' => 1,
                'text' => $text
            ];
        }
    }
}

echo json_encode($result);