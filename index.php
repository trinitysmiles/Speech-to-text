<?php
require __DIR__ . '/src/Database.php';
require __DIR__ . '/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Speech to text</title>
</head>
<body>
<form onsubmit="return addNewSpeech()">
    <input type="text" id="link" name="link" placeholder="paste url">
    <input type="submit" value="submit">
</form>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">FilePath</th>
        <th scope="col">Text</th>
        <th scope="col">Created_at</th>
        <th scope="col">Converted_at</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($db->select('speech')->result() as $result) {
        ?>
        <tr>
            <th scope="row"><?php echo $result->id ?></th>
            <td><?php echo $result->filename ?></td>
            <td><?php echo $result->text ?></td>
            <td><?php echo $result->created_at ?></td>
            <td><?php echo $result->updated_at ?></td>
            <td>
                <button type="button"
                        class="btn btn-outline-primary "
                        onclick="convert(<?php echo $result->id ?>)"><?php echo $result->text !== '' ? 'Convert again' : 'Convert' ?></button>
            </td>

        </tr>
        <?php
    } ?>
    </tbody>
</table>
<style>
    *, ::after, ::before {
        box-sizing: border-box;
    }

    table {
        border-collapse: collapse;
    }

    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
    }

    th {
        text-align: inherit;
    }

    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }

    .table-striped tbody tr:nth-of-type(odd) {
        background-color: rgba(0, 0, 0, .05);
    }

</style>
<script>
    function addNewSpeech() {
        ajax.post('add.php', {link: document.getElementById('link').value}, function (el) {
            var result = JSON.parse(el);
            if (result.status === 1) {
                setTimeout(function () {
                    location.reload();
                }, 700)
            } else {
                alert('fail')
            }
        });
        return false;
    }

    function convert(id) {
        ajax.post('convert.php', {id: id}, function (el) {
            var result = JSON.parse(el);
            if (result.status === 1) {
                setTimeout(function () {
                    location.reload();
                }, 700)
            } else {
                alert(result.text)
            }
        });

    }

    var ajax = {};
    ajax.x = function () {
        if (typeof XMLHttpRequest !== 'undefined') {
            return new XMLHttpRequest();
        }
        var versions = [
            "MSXML2.XmlHttp.6.0",
            "MSXML2.XmlHttp.5.0",
            "MSXML2.XmlHttp.4.0",
            "MSXML2.XmlHttp.3.0",
            "MSXML2.XmlHttp.2.0",
            "Microsoft.XmlHttp"
        ];
        var xhr;
        for (var i = 0; i < versions.length; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            } catch (e) {
            }
        }
        return xhr;
    };
    ajax.send = function (url, callback, method, data, async) {
        if (async === undefined) {
            async = true;
        }
        var x = ajax.x();
        x.open(method, url, async);
        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                callback(x.responseText)
            }
        };
        if (method === 'POST') {
            x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        x.send(data)
    };
    ajax.get = function (url, data, callback, async) {
        var query = [];
        for (var key in data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
    };
    ajax.post = function (url, data, callback, async) {
        var query = [];
        for (var key in data) {
            query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        ajax.send(url, callback, 'POST', query.join('&'), async)
    };
</script>
</body>
</html>